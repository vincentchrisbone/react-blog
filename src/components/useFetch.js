import { useEffect,useState } from "react";
const useFetch = (url) => {
    const [data, setData] = useState(null);
    const [isLoading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    useEffect(() => {
        const abortConts = new AbortController();
        
        setTimeout(() => {
            fetch(url)
                .then(res => {
                    if (!res.ok) throw Error('Could not find that resource');
                    return res.json();
                })
                .then(data => {
                    setData(data);
                    setLoading(false);
                    setError(null);
                }).catch(err => {
                    if (err.name === "AbortError") {
                        console.log("Abort")
                    } else {
                        setError(err.message);
                    setLoading(false);
                    }
                });
        }, 1000)
         return () => abortConts.abort();
    }, [url]);
    return {data,isLoading,error}
}

export default useFetch;