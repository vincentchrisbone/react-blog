 import { Navbar, Container, Nav, NavDropdown } from 'react-bootstrap';
const Narbar = () => {
    const date = new Date();
    return (
        <>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" sticky="top">
            <Container>
            <Navbar.Brand href="/">Vincent`s Blogs</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="me-auto">
                <Nav.Link href="/">Home</Nav.Link>
                <Nav.Link href="/create">New Blog</Nav.Link>
                </Nav>
                <Nav>
                <Nav.Link href="/about">About Us</Nav.Link>
                <Nav.Link eventKey={2} href="/contact">
                   Contact Us
                </Nav.Link>
                </Nav>
            </Navbar.Collapse>
            </Container>
            </Navbar>
            <div className="container">
                <span>Date: {date.getFullYear()} - {date.getMonth()}-{date.getDate()} {date.getHours()}:
                {date.getMinutes()}:{date.getSeconds()}  </span>
            </div>
        </>
    );
}
 
export default Narbar;