import { Link } from "react-router-dom"

const PageNotFound = () => {
    return (
        <div className="container text-center">
            <div className="text-danger">
                <h2>Sorry</h2>
                <p>This page is not found</p>
            </div>
            <Link to="/">back to homepage.....</Link>
        </div>
     );
}
 
export default PageNotFound;