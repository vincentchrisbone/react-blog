import { Link } from "react-router-dom";
const BlogList = ({ blogs, title}) => {
    return (
        <div className="container">
            {
                blogs.map((blog) => (
                    <div className="card-deck mb-2" key={blog.id}>
                        <div className="card">
                            <Link to={`/blogs/${blog.id}`} className="nav-link">
                                <div className="card-body">
                                <h4 className="card-title text-dark">{blog.title}</h4>
                                <p className="card-text">Written by {blog.author}</p>
                                </div>
                            </Link>
                        </div>
                    </div>
                ))
                }
        </div>
    );
}
 
export default BlogList;