import { useState } from "react";
import { useHistory } from "react-router-dom";

const Create = () => {
    const [title, setTitle] = useState('');
    const [body, setBody] = useState('');
    const [author, setAuthor] = useState('Vincent Tetteh');
    const [isLoading, setLoading] = useState(false);
    const history = useHistory();
    const handleSubmit = (e) => {
        e.preventDefault();
        const blog = { title, body, author };
        setLoading(true);
        setTimeout(() => {
            fetch('https://jsonplaceholder.typicode.com/posts', {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(blog)
            }).then(() => {
                setLoading(false);
                console.log("new blog added");
                history.push('/');
            });
        },1000)
    }

    return (
        <div className="container row d-flex justify-content-center ">
            <h2 className="text-center text-dark">Add a blog</h2>
            <form className="justify-content-center" onSubmit={handleSubmit}>
                <div className="mb-3 col-sm-4 col-md-6 col-xs-4">
                    <label className="form-label">Blog Title</label>
                    <input
                        type="text"
                        required
                        className="form-control"
                        placeholder="Blog title"
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}
                    />
                </div>
                <div className="mb-3 col-sm-4 col-md-6 col-xs-4" >
                    <label className="form-label">Blog body</label>
                    <textarea
                        required
                        className="form-control"
                        value={body}
                        onChange={(e) => setBody(e.target.value)}
                    >
                        
                    </textarea>
                </div>
                <div className="mb-3 col-sm-4">
                    <select
                        className="form-select"
                        value={author}
                        onChange={(e) => setAuthor(e.target.value)}
                    >
                        <option value="Vincent Tetteh">Vincent Tetteh</option>
                        <option value="Kwame Tetteh">Kwame Tetteh</option>
                        <option value="Gify Tetteh">Gifty Tetteh</option>
                    </select>
                </div>
                <div className="mb-3">
                    {!isLoading && <button className="btn btn-primary">Add blog</button>}
                    {isLoading && (
                        <button className="btn btn-primary" type="button" disabled>
                            <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            <span className="sr-only">Loading...</span>
                        </button>
                    )}
                </div>
            </form>
        </div>
     );
}
 
export default Create;