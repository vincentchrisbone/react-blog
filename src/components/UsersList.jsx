const UsersList = ({users}) => {
    return (
        <div className="container">
            {
                users.map(user => (
                    <div class="card" key={user.id}>
                        <div class="card-body">
                            <h4 class="card-title">{user.firstName} {user.lastName}</h4>
                            <p class="card-text">{user.age}</p>
                        </div>
                    </div>
                ))
            }
        </div>
     );
}
 
export default UsersList;