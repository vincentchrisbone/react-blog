import { useState, useEffect } from "react";
import UsersList from "./UsersList";
const Users = () => {
    const [users, setUsers] = useState();
    useEffect(() => {
        fetch('http://localhost:5000/users')
            .then(res => {
                return res.json();
            })
            .then(data => {
                setUsers(data);
        })
    },[])

    return (
        <div className="container">
            {!users && <p>Loading....</p>}
            {users && <UsersList users={users}/>}
        </div>
    );
}
 
export default Users;