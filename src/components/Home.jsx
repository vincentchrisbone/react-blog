import BlogList from "./BlogList";
import Spinner from "./Spinner";
import useFetch from "./useFetch";
import Users from "./Users";
const Home = () => {
    const { data:blogs, isLoading, error } = useFetch('https://jsonplaceholder.typicode.com/posts')
    return (
        <>
            {isLoading && <Spinner />}
            {error && <div className="container" style={{color:"red"}}> {error}</div>}
            {blogs && !error && <BlogList blogs={blogs} title="All Blogs"/>}
            {/* <Users /> */}
        </>
    );
}
 
export default Home;