const Spinner = () => {
    return (
       <div className="text-center mt-4">
            <div className="spinner-border" role="status">
            </div>
        </div>
     );
}
 
export default Spinner;