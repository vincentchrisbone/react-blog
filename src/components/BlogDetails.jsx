import { useParams } from "react-router-dom";
import Spinner from "./Spinner";
import useFetch from "./useFetch";
import { useHistory } from "react-router-dom";

const BlogDetails = () => {
    const { id } = useParams();
    const { data: blog, error, isLoading } = useFetch('https://jsonplaceholder.typicode.com/posts/' + id);
    const history = useHistory();

    const handleDelete = () => {
        fetch('https://jsonplaceholder.typicode.com/posts/' + blog.id, {
            method: "DELETE"
        }).then(() => {
            console.log(`${blog.title} deleted successfully`);
            history.push('/');
        });
    }
    
    return (
        <div className="container">
            {isLoading && <Spinner />}
            {error && <div> {error}</div>}
            {blog && (
                <>
                    <article>
                        <h2>{ blog.title }</h2>
                        <p>Written by { blog.author }</p>
                        <div>{ blog.body }</div>
                    </article>
                    <button className="btn btn-danger mt-3" onClick={handleDelete}>Delete</button>
                </>
                
            )}
        </div>
      );
}
 
export default BlogDetails;